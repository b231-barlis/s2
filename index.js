//create student one
let studentOneName = "Tony";
let studentOneEmail = "starksindustries@mail.com";
let studentOneGrades = [89, 84, 78, 88];

//create student two
let studentTwoName = "Peter";
let studentTwoEmail = "spideyman@mail.com";
let studentTwoGrades = [78, 82, 79, 85];

//create student three
let studentThreeName = "Wanda";
let studentThreeEmail = "scarlettMaximoff@mail.com";
let studentThreeGrades = [87, 89, 91, 93];

//create student four
let studentFourName = "Steve";
let studentFourEmail = "captainRogers@mail.com";
let studentFourGrades = [91, 89, 92, 93];

//actions that students may perform will be lumped together
function login(email) {
  console.log(`${email} has logged in`);
}

function logout(email) {
  console.log(`${email} has logged out`);
}

function listGrades(grades) {
  grades.forEach((grade) => {
    console.log(grade);
  });
}

let studentOne = {
  name: `Tony`,
  email: `starksindustries@mail.com`,
  grades: [89, 84, 78, 88],

  login(email) {
    console.log(`${this.email} has logged in`);
  },
  logout(email) {
    console.log(`${this.email} has logged out`);
  },
  computeAve() {
    let sum = 0;
    this.grades.forEach((grade) => (sum = sum + grade));
    return sum / 4;
  },

  willPass() {
    return this.computeAve() >= 85 ? true : false;
  },
  willPassWithHonors() {
    return this.willPass() && this.computeAve() >= 90 ? true : false;
  },
};

console.log(studentOne.willPass(), studentOne.computeAve());

let studentTwo = {
  name: `Peter`,
  email: `spideyman@mail.com`,
  grades: [78, 82, 79, 85],

  login(email) {
    console.log(`${this.email} has logged in`);
  },
  logout(email) {
    console.log(`${email} has logged out`);
  },
  computeAve() {
    let sum = 0;
    this.grades.forEach((grade) => (sum = sum + grade));
    return sum / 4;
  },

  willPass() {
    return this.computeAve() >= 85 ? true : false;
  },
  willPassWithHonors() {
    return this.willPass() && this.computeAve() >= 90 ? true : false;
  },
};

let studentThree = {
  name: `Wanda`,
  email: `scarlettMaximoff@mail.com`,
  grades: [87, 89, 91, 93],

  login(email) {
    console.log(`${email} has logged in`);
  },
  logout(email) {
    console.log(`${email} has logged out`);
  },
  computeAve() {
    let sum = 0;
    this.grades.forEach((grade) => (sum = sum + grade));
    return sum / 4;
  },

  willPass() {
    return this.computeAve() >= 85 ? true : false;
  },
  willPassWithHonors() {
    return this.willPass() && this.computeAve() >= 90 ? true : false;
  },
};

let studentFour = {
  name: `Steve`,
  email: `captainRogers@mail.com@mail.com`,
  grades: [91, 89, 92, 93],

  login(email) {
    console.log(`${email} has logged in`);
  },
  logout(email) {
    console.log(`${this.email} has logged out`);
  },
  computeAve() {
    let sum = 0;
    this.grades.forEach((grade) => (sum = sum + grade));
    return sum / 4;
  },

  willPass() {
    return this.computeAve() >= 85 ? true : false;
  },
  willPassWithHonors() {
    return this.willPass() && this.computeAve() >= 90 ? true : false;
  },
};

const classofA1 = {
  students: [studentOne, studentTwo, studentThree, studentFour],

  countHonorStudents() {
    let result = 0;
    this.students.forEach((student) => {
      if (student.willPassWithHonors()) {
        result++;
      }
    });
    return result;
  },
  honorPercentage() {
    return (this.countHonorStudents() / this.students.length) * 100;
  },
  retrieveHonorStudentInfo() {
    const arr = this.students.map((student) => {
      return {
        email: student.email,
        aveGrade: student.computeAve(student.grades),
      };
    });
    return arr;
  },
};

console.log(classofA1.retrieveHonorStudentInfo());

/*
1. What is the term given to unorganized code that's very hard to work with?
 Spaghetti code

2. How are object literals written in JS?
 Using curly brackets

3. What do you call the concept of organizing information and functionality to belong to an object?
Encapsulation

4. If student1 has a method named enroll(), how would you invoke it?
studentOne.enroll()

5. True or False: Objects can have objects as properties. True

6. What does the this keyword refer to if used in an arrow function method?
   Global window object

7. True or False: A method can have no parameters and still work.
    True

8. True or False: Arrays can have objects as elements.
   True

9. True or False: Arrays are objects.
   True

10. True or False: Objects can have arrays as properties.
    True

*/
